# Personal laptop setup

Install
- [x] docker
- [x] docker compose
- [x] virt manager
- [x] vs code
- [x] git / git alias
- [x] zsh
- [x] terminator
- [x] desktop theme
- [x] thunderbird mail
- [x] nvm / node
- [x] sdk man
- [x] my bash alias
- [x] kubctl
- [x] k9s
